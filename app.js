const express = require("express");
const app = express();
const converter = require("./models/Converter");

app.get("/", (req, res) =>
  res.send(
    "API para conversão de temperatura entre Celsius e Fahrenheit. Exemplo: /converter/?c=1"
  )
);
app.get("/temp", (req, res) => {
  process.stdout.write(
    "\nRECEIVE => celsius: " + req.query.c + " / fahrenheit: " + req.query.f
  );

  var response = converter.execute(req.query);

  process.stdout.write("\nRESPONSE => " + JSON.stringify(response));

  res.send(response);
});

app.listen(3001);
