module.exports = {
  execute: params => {
    var celsius = params.c;
    var fahrenheit = params.f;

    if (
      (celsius == undefined || celsius == "") &&
      (fahrenheit == undefined || fahrenheit == "")
    ) {
      return { c: 0, f: 0 };
    }

    if (celsius && celsius != "") {
      fahrenheit = 1.8 * celsius + 32;
    } else {
      celsius = (fahrenheit - 32) / 1.8;
    }

    return { c: celsius, f: fahrenheit };
  }
};
