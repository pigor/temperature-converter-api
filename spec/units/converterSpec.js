const app = require("../../models/Converter");

describe("converter", function() {
  it("celsius to fahrenheit", function() {
    const result = app.execute({ c: 38 });

    expect(result.c).toBe(38);
    expect(result.f).toBe(100.4);
  });

  it("farenheit to celsius", function() {
    const result = app.execute({ f: 100.4 });

    expect(result.c).toBe(38);
    expect(result.f).toBe(100.4);
  });

  it("empty values", function() {
    const result = app.execute({});

    expect(result.c).toBe(0);
    expect(result.f).toBe(0);
  });
});
